package sr.bedrijf.app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sr.bedrijf.app.dao.factory.DAOFactory;
import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.dao.interfaces.FunctieDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerDAO;
import sr.bedrijf.app.domain.Afdeling;
import sr.bedrijf.app.domain.Functie;
import sr.bedrijf.app.domain.Rol;
import sr.bedrijf.app.domain.Werknemer;

public class Start {

	public static void main(String[] args) {
		DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		
		//DAOFactory mysqlFactory = DAOFactory.getDAOFactory(0);
        WerknemerDAO werknemerDAO = mysqlFactory.getWerknemerDAO();
        AfdelingDAO afdelingDAO = mysqlFactory.getAfdelingDAO();
        FunctieDAO functieDAO = mysqlFactory.getFunctieDAO();
         
        // CREATE
        //testCreateWerknemer(werknemerDAO, afdelingDAO, functieDAO);
        //testCreateAfdeling(afdelingDAO);
        //testCreateFunctie(functieDAO);
        
        // READ
        //testRead(werknemerDAO);
    	
        // UPDATE
        //werknemerDAO.update(werknemer);
         
          
        // DELETE
        //werknemerDAO.delete(id);

	}

	private static void testCreateFunctie(FunctieDAO functieDAO) {
		Functie nieuweFunctie = new Functie();
        nieuweFunctie.setNaam("assistent sec officer");
        int nieuwFunctieId = functieDAO.create(nieuweFunctie);
        System.out.println("Nieuw id van Functie: " + nieuwFunctieId);
	}

	private static void testCreateAfdeling(AfdelingDAO afdelingDAO) {
		Afdeling nieuweAfdeling = new Afdeling();
        nieuweAfdeling.setNaam("Techn. Dienst");
        int nieuwAfdelingId = afdelingDAO.create(nieuweAfdeling);
        System.out.println("Nieuw id van afdeling: " + nieuwAfdelingId);
	}

	private static void testCreateWerknemer(WerknemerDAO werknemerDAO,
			AfdelingDAO afdelingDAO, FunctieDAO functieDAO) {
		Werknemer werknemer = new Werknemer();
        werknemer.setVoornaam("Johannes");
        werknemer.setAchternaam("Dedoper");
        werknemer.setExtentie("3420");
        werknemer.setTelefoon("082669832");

        // Lees de Afdeling object uit welk behoort bij de Afdeling ICT
        Afdeling ictAfdeling = afdelingDAO.read(1);
        
        // Lees de Functie object uiut welke behoort bij de Functie programmer
        Functie programmerFunctie = functieDAO.read(3);
        
        werknemer.setAfdeling(ictAfdeling);
        werknemer.setFunctie(programmerFunctie);
        
        int nieuwId = werknemerDAO.create(werknemer);
        System.out.println("Nieuw id van werknemer: " + nieuwId);
	}

	private static void testRead(WerknemerDAO werknemerDAO) {
		int id =  Integer.parseInt("1");
        
        Werknemer werknemer = werknemerDAO.read(id);
        System.out.println("De werknemer info is " + 
        		"\n\tid: " + werknemer.getId() + 
        		"\n\tVoornaam:" + werknemer.getVoornaam() + 
        		"\n\tAchternaam:" + werknemer.getAchternaam() +
        		"\n\tAfdeling:" + ((werknemer.getAfdeling() != null) ? werknemer.getAfdeling().getNaam() : "null"));
        
        List<Rol> deRollen = new ArrayList<Rol>();
        deRollen = werknemer.getRollen();
        
        Iterator<Rol> iterator = deRollen.iterator();
    	while (iterator.hasNext()) {
    		Rol rol = new Rol();
    		rol = iterator.next();
    		System.out.println("De rol id: " + rol.getId() + " .De rol naam: " + rol.getNaam());
        	
        }
        
    	System.out.println("////////   //////////////////");
    	System.out.println("////////   //////////////////");
    	System.out.println("////////   //////////////////");

    	
    	for (int i=0;i< deRollen.size() ; i++){
    		Rol rol = new Rol();
    		rol= deRollen.get(i);
    		System.out.println("De rol id2: " + rol.getId() + " .De rol naam2: " + rol.getNaam());
    		
    	}
	}

}
