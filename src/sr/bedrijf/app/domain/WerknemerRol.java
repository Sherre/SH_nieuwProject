package sr.bedrijf.app.domain;

public class WerknemerRol {

	private int werknemerId;
	private int rolId;
	
	public int getWerknemerId() {
		return werknemerId;
	}
	public void setWerknemerId(int werknemerId) {
		this.werknemerId = werknemerId;
	}
	public int getRolId() {
		return rolId;
	}
	public void setRolId(int rolId) {
		this.rolId = rolId;
	}
	
	
	
	
}
