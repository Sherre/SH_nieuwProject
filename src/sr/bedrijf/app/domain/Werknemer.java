package sr.bedrijf.app.domain;

import java.util.ArrayList;
import java.util.List;

public class Werknemer {
	
    private int id;
    
    private String achternaam;
    
    private String voornaam;
	
    private Afdeling afdeling;
    
    private String extentie;
    
    private String telefoon;
    
    private Functie functie;
    
    private List<Rol> rollen;
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getExtentie() {
		return extentie;
	}

	public void setExtentie(String extentie) {
		this.extentie = extentie;
	}

	public String getTelefoon() {
		return telefoon;
	}

	public void setTelefoon(String telefoon) {
		this.telefoon = telefoon;
	}

	public Afdeling getAfdeling() {
		return afdeling;
	}

	public void setAfdeling(Afdeling afdeling) {
		this.afdeling = afdeling;
	}

	public Functie getFunctie() {
		return functie;
	}

	public void setFunctie(Functie functie) {
		this.functie = functie;
	}

	public List<Rol> getRollen() {
		return rollen;
	}

	public void setRollen(List<Rol> rollen) {
		this.rollen = rollen;
	}
	
	public void addRol(Rol rol){
		if (rollen == null){
			rollen = new ArrayList<Rol>();
		}
		rollen.add(rol);
	}
}
