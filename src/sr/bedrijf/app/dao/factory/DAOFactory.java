package sr.bedrijf.app.dao.factory;

import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.dao.interfaces.FunctieDAO;
import sr.bedrijf.app.dao.interfaces.RolDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerRolDAO;

public abstract class DAOFactory {
	 
    /** Static member for mysql-factory. */
    public static final int MYSQL = 0;  
    /** Static member for derby-factory. */
    public static final int DERBY = 1;
 
    /** Abstract method for the RiverDAO. */
    public abstract AfdelingDAO getAfdelingDAO();
    /** Abstract method for the AfdelingDAO. */
    public abstract WerknemerDAO getWerknemerDAO();
    
    public abstract FunctieDAO getFunctieDAO();
    
    public abstract WerknemerRolDAO getWerknemerRolDAO();
    
    public abstract RolDAO getRolDAO();
 
    /**
     * Factory-method
     * 
     * @param database
     *            the database to choose
     * @return a matching factory
     */
    public static DAOFactory getDAOFactory(int database) {
        switch (database) {
        case MYSQL:
            return new MysqlDAOFactory();
        case DERBY:
             return new DerbyDAOFactory();
        default:
            return null;
        }
    }
	
}