package sr.bedrijf.app.dao.factory;

import sr.bedrijf.app.dao.factory.DAOFactory;
import sr.bedrijf.app.dao.impl.mysql.MysqlFunctieDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlRolDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlWerknemerRolDAO;
import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.dao.interfaces.FunctieDAO;
import sr.bedrijf.app.dao.interfaces.RolDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerRolDAO;

public class DerbyDAOFactory extends DAOFactory {

	@Override
	public AfdelingDAO getAfdelingDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WerknemerDAO getWerknemerDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FunctieDAO getFunctieDAO() {
		// TODO Auto-generated method stub
		return new MysqlFunctieDAO();
	}
	
	@Override
	public WerknemerRolDAO getWerknemerRolDAO() {
		// TODO Auto-generated method stub
		return new MysqlWerknemerRolDAO();
	}
	
	@Override
	public RolDAO getRolDAO() {
		// TODO Auto-generated method stub
		return new MysqlRolDAO();
	}

}
