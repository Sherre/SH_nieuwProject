package sr.bedrijf.app.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import org.apache.log4j.Logger;

import sr.bedrijf.app.dao.impl.mysql.MysqlAfdelingDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlFunctieDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlRolDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlWerknemerDAO;
import sr.bedrijf.app.dao.impl.mysql.MysqlWerknemerRolDAO;
import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.dao.interfaces.FunctieDAO;
import sr.bedrijf.app.dao.interfaces.RolDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerRolDAO;

public class MysqlDAOFactory extends DAOFactory {

	/** The logger. */
	//private static final Logger logger = Logger
	//        .getLogger(MysqlDAOFactory.class);

	/** The driver-class. */
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	/** The url to database. */
	public static final String DBURL = "jdbc:mysql://localhost/telefoongids";
	/** The username for database-operations. */
	public static final String USER = "dbuser";
	/** The password for database-operations. */
	public static final String PASS = "dbuser";

	/**
	 * Method to create a Connection on the mysql-database.
	 * 
	 * @return the Connection.
	 */
	public static Connection createConnection() {
		Connection conn = null;
		try {
			if (conn == null){
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(DBURL, USER, PASS);
				//conn = DriverManager.getConnection("jdbc:mysql://localhost/telefoongids",
				//"dbuser",
				//"dbuser");
			}
		} catch (SQLException e) {
			//logger.error(e.getMessage());
			System.out.println("error 1");
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			//logger.error(e.getMessage());
			System.out.println("error 2");
			System.out.println(e.getMessage());
		}
		return conn;
	}

	@Override
	public AfdelingDAO getAfdelingDAO() {
		// TODO Auto-generated method stub
		return new MysqlAfdelingDAO();
	}

	@Override
	public WerknemerDAO getWerknemerDAO() {
		// TODO Auto-generated method stub
		return new MysqlWerknemerDAO();
	}

	@Override
	public FunctieDAO getFunctieDAO() {
		// TODO Auto-generated method stub
		return new MysqlFunctieDAO();
	}
	
	@Override
	public WerknemerRolDAO getWerknemerRolDAO() {
		// TODO Auto-generated method stub
		return new MysqlWerknemerRolDAO();
	}
	
	@Override
	public RolDAO getRolDAO() {
		// TODO Auto-generated method stub
		return new MysqlRolDAO();
	}


}
