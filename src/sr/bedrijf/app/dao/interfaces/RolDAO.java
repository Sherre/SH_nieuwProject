package sr.bedrijf.app.dao.interfaces;



import java.util.List;

import sr.bedrijf.app.domain.Rol;

public interface RolDAO {
	
	/** Creates a Rol and returns the id. */
    public int create(Rol rol);
 
    /** Receives a Rol by given id. */
    public Rol read(int id);
    
    
    /** Updates an existing Rol. */
    public boolean update(Rol rol);
 
    /** Deletes a Rol by id. */
    public boolean delete(int id);


/** Receives a List of Rol by given werknemer id. */
public List<Rol> readRollen(int werknemerId);
}