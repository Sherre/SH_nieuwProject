package sr.bedrijf.app.dao.interfaces;

import sr.bedrijf.app.domain.Functie;

public interface FunctieDAO {
	/** Creates a Functie and returns the id. */
    public int create(Functie functie);
 
    /** Receives a Functie by given id. */
    public Functie read(int id);
 
    /** Updates an existing Functie. */
    public boolean update(Functie functie);
 
    /** Deletes a Functie by id. */
    public boolean delete(int id);
}
