package sr.bedrijf.app.dao.interfaces;

import sr.bedrijf.app.domain.Afdeling;

public interface AfdelingDAO {
	
	/** Creates a Afdeling and returns the id. */
    public int create(Afdeling afdeling);
 
    /** Receives a Afdeling by given id. */
    public Afdeling read(int id);
 
    /** Updates an existing Afdeling. */
    public boolean update(Afdeling afdeling);
 
    /** Deletes a Afdeling by id. */
    public boolean delete(int id);
}
