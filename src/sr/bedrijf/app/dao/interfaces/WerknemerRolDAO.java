package sr.bedrijf.app.dao.interfaces;


import java.util.List;

import sr.bedrijf.app.domain.WerknemerRol;

public interface WerknemerRolDAO {
	
	/** Creates a WerknemerRol and returns the id. */
    public int create(WerknemerRol werknemerRol);
 
    /** Receives a List of WerknemerRol by given werknemerId. */
    public List<WerknemerRol> read(int werknemerId);
    
    /** Updates an existing Rol. */
    public boolean update(WerknemerRol werknemerRol);
 
    /** Deletes a WerknemerRol by id. */
    public boolean delete(int id);
}
