package sr.bedrijf.app.dao.interfaces;

import sr.bedrijf.app.domain.Werknemer;

public interface WerknemerDAO {
	/** Creates a Werknemer and returns the id. */
    public int create(Werknemer werknemer);
 
    /** Receives a Werknemer by given id. */
    public Werknemer read(int id);
 
    /** Updates an existing Werknemer. */
    public boolean update(Werknemer werknemer);
 
    /** Deletes a Werknemer by id. */
    public boolean delete(int id);
}
