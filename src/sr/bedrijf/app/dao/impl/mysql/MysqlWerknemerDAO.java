package sr.bedrijf.app.dao.impl.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import sr.bedrijf.app.dao.factory.DAOFactory;
import sr.bedrijf.app.dao.factory.MysqlDAOFactory;
import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.dao.interfaces.FunctieDAO;
import sr.bedrijf.app.dao.interfaces.RolDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerDAO;
import sr.bedrijf.app.dao.interfaces.WerknemerRolDAO;
import sr.bedrijf.app.domain.Afdeling;
import sr.bedrijf.app.domain.Werknemer;

public class MysqlWerknemerDAO implements WerknemerDAO{

	private static final String CREATE_QUERY = "INSERT INTO werknemer (achternaam, " +
	"voornaam, extentie, telefoon, afdeling, functie) VALUES (?,?,?,?,?,?)";
	/** The query for read. */
	private static final String READ_QUERY = "SELECT * FROM werknemer WHERE id = ?";
	/** The query for update. */
	private static final String UPDATE_QUERY = "UPDATE werknemer SET voornaam=? , achternaam=? WHERE id = ?";
	/** The query for delete. */
	private static final String DELETE_QUERY = "DELETE FROM werknemer WHERE id = ?";


	@Override
	public int create(Werknemer werknemer) {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		int nieuwWerknemerId = -1;
		try {
			conn = MysqlDAOFactory.createConnection();
			preparedStatement = conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, werknemer.getAchternaam());
			preparedStatement.setString(2, werknemer.getVoornaam());
			preparedStatement.setString(3, werknemer.getExtentie());
			preparedStatement.setString(4, werknemer.getTelefoon());

			if (werknemer.getAfdeling() == null){
				preparedStatement.setNull(5, java.sql.Types.NULL);
			}else{
				preparedStatement.setInt(5, werknemer.getAfdeling().getId());
			}

			if (werknemer.getFunctie() == null){
				preparedStatement.setNull(6, java.sql.Types.NULL);
			}else{
				preparedStatement.setInt(6, werknemer.getFunctie().getId());
			}


			preparedStatement.execute(); 
			// de executeUpdate methode is eigenlijk van toepassing voor DML

			result = preparedStatement.getGeneratedKeys();

			if ((result != null) && (result.next())) {
				nieuwWerknemerId = (int)result.getLong(1);
			}
		} catch (SQLException e) {
			//logger.error(e.getMessage());
			System.out.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (Exception rse) {
				//logger.error(rse.getMessage());
				System.out.println(rse.getMessage());
			}
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				//logger.error(sse.getMessage());
				System.out.println(sse.getMessage());
			}
			try {
				conn.close();
			} catch (Exception cse) {
				//logger.error(cse.getMessage());
				System.out.println(cse.getMessage());
			}
		}

		return nieuwWerknemerId;
	}

	@Override
	public Werknemer read(int id) {
		Werknemer werknemer = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = MysqlDAOFactory.createConnection();
			preparedStatement = conn.prepareStatement(READ_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				werknemer = new Werknemer();
				werknemer.setId(result.getInt("id"));
				werknemer.setVoornaam(result.getString("voornaam"));
				werknemer.setAchternaam(result.getString("achternaam"));
				werknemer.setExtentie(result.getString("extentie"));
				werknemer.setTelefoon(result.getString("telefoon"));

				//lees de werknemer id
				int werknemerId = result.getInt("id");

				// Bepaal de id van de afdeling 
				int afdId = result.getInt("afdeling");

				// Bepaal de id van de functie 
				int functieId = result.getInt("functie");

				// Haal de DAO Factory op voor Mysql
				DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);

				// Haal referentie op naar de afdelingDAO
				AfdelingDAO afdelingDAO = mysqlFactory.getAfdelingDAO();

				// Haal referentie op naar de functieDAO
				FunctieDAO functieDAO = mysqlFactory.getFunctieDAO();

				// Haal referentie op naar de werknemerRolDAO
				RolDAO rolDAO = mysqlFactory.getRolDAO();

				werknemer.setAfdeling(afdelingDAO.read(afdId));

				werknemer.setFunctie(functieDAO.read(functieId));

				werknemer.setRollen(rolDAO.readRollen(werknemerId));

			} else {
				// TODO
			}
		} catch (SQLException e) {
			//logger.error(e.getMessage());
			System.out.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (Exception rse) {
				//logger.error(rse.getMessage());
				System.out.println(rse.getMessage());
			}
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				//logger.error(sse.getMessage());
				System.out.println(sse.getMessage());
			}
			try {
				conn.close();
			} catch (Exception cse) {
				//logger.error(cse.getMessage());
				System.out.println(cse.getMessage());
			}
		}

		return werknemer;
	}

	@Override
	public boolean update(Werknemer werknemer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
