package sr.bedrijf.app.dao.impl.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sr.bedrijf.app.dao.factory.MysqlDAOFactory;
import sr.bedrijf.app.dao.interfaces.WerknemerRolDAO;
import sr.bedrijf.app.domain.Afdeling;
import sr.bedrijf.app.domain.WerknemerRol;

public class MysqlWerknemerRolDAO implements WerknemerRolDAO{
	
	/** The query for read. */
    private static final String READ_QUERY = "SELECT * FROM WerknemerRol WHERE werknemerid = ?";

	@Override
	public int create(WerknemerRol werknemerRol) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<WerknemerRol> read(int werknemerId) {

		WerknemerRol werknemerRol = null;
		List<WerknemerRol> werknemerRollen = new ArrayList<WerknemerRol>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = MysqlDAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1, werknemerId);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
            	werknemerRol = new WerknemerRol();
            	werknemerRol.setWerknemerId(result.getInt("werknemerid"));
            	werknemerRol.setRolId(result.getInt("rolid"));
            	
            	//add the werknemerRol to the List of Class WerknemerRol
            	werknemerRollen.add(werknemerRol);
            	
            }
        } catch (SQLException e) {
            //logger.error(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                //logger.error(rse.getMessage());
                System.out.println(rse.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                //logger.error(sse.getMessage());
                System.out.println(sse.getMessage());
            }
            try {
                conn.close();
            } catch (Exception cse) {
                //logger.error(cse.getMessage());
                System.out.println(cse.getMessage());
            }
        }
 
        return werknemerRollen;
	}

	@Override
	public boolean update(WerknemerRol werknemerRol) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
