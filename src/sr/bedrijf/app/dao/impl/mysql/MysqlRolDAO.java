package sr.bedrijf.app.dao.impl.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sr.bedrijf.app.dao.factory.MysqlDAOFactory;
import sr.bedrijf.app.dao.interfaces.RolDAO;
import sr.bedrijf.app.domain.Rol;
import sr.bedrijf.app.domain.WerknemerRol;

public class MysqlRolDAO implements RolDAO{
	
	/** The query for read. */
    private static final String READ_QUERY = "SELECT * FROM rol WHERE id = ?";
    
    private static final String READ_Rollen_QUERY = "select ro.* from rol ro, werknemerrol wer where wer.rolid = ro.id and wer.werknemerid = ?";
    
	
	@Override
	public int create(Rol rol) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Rol read(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Rol> readRollen(int werknemerId) {


		Rol rol = null;
		List<Rol> rollen = new ArrayList<Rol>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = MysqlDAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_Rollen_QUERY);
            preparedStatement.setInt(1, werknemerId);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
            
            while (result.next() ) {
            	rol = new Rol();
            	rol.setId(result.getInt("id"));
            	rol.setNaam(result.getString("naam"));
            	//add the werknemerRol to the List of Class WerknemerRol
            	rollen.add(rol);
            	
            }
        } catch (SQLException e) {
            //logger.error(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                //logger.error(rse.getMessage());
                System.out.println(rse.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                //logger.error(sse.getMessage());
                System.out.println(sse.getMessage());
            }
            try {
                conn.close();
            } catch (Exception cse) {
                //logger.error(cse.getMessage());
                System.out.println(cse.getMessage());
            }
        }
 
        return rollen;

	}
	
	@Override
	public boolean update(Rol rol) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
