package sr.bedrijf.app.dao.impl.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import sr.bedrijf.app.dao.factory.MysqlDAOFactory;
import sr.bedrijf.app.dao.interfaces.AfdelingDAO;
import sr.bedrijf.app.domain.Afdeling;

public class MysqlAfdelingDAO implements AfdelingDAO{
	
	private static final String CREATE_QUERY = "INSERT INTO afdeling (naam) VALUES (?)";
	
    private static final String READ_QUERY = "SELECT * FROM afdeling WHERE id = ?";
    
	@Override
	public int create(Afdeling afdeling) {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		int nieuwAfdelingId = -1;
		try {
			conn = MysqlDAOFactory.createConnection();
			preparedStatement = conn.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, afdeling.getNaam());

			preparedStatement.execute(); 
			// de executeUpdate methode is eigenlijk van toepassing voor DML

			result = preparedStatement.getGeneratedKeys();

			if ((result != null) && (result.next())) {
				nieuwAfdelingId = (int)result.getLong(1);
			}
		} catch (SQLException e) {
			//logger.error(e.getMessage());
			System.out.println(e.getMessage());
		} finally {
			try {
				result.close();
			} catch (Exception rse) {
				//logger.error(rse.getMessage());
				System.out.println(rse.getMessage());
			}
			try {
				preparedStatement.close();
			} catch (Exception sse) {
				//logger.error(sse.getMessage());
				System.out.println(sse.getMessage());
			}
			try {
				conn.close();
			} catch (Exception cse) {
				//logger.error(cse.getMessage());
				System.out.println(cse.getMessage());
			}
		}

		return nieuwAfdelingId;
	}

	@Override
	public Afdeling read(int id) {
		Afdeling afdeling = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = MysqlDAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
            	afdeling = new Afdeling();
            	afdeling.setId(result.getInt("id"));
            	afdeling.setNaam(result.getString("naam"));
            }
        } catch (SQLException e) {
            //logger.error(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                //logger.error(rse.getMessage());
                System.out.println(rse.getMessage());
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                //logger.error(sse.getMessage());
                System.out.println(sse.getMessage());
            }
            try {
                conn.close();
            } catch (Exception cse) {
                //logger.error(cse.getMessage());
                System.out.println(cse.getMessage());
            }
        }
 
        return afdeling;
	}

	@Override
	public boolean update(Afdeling afdeling) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
