-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for telefoongids
CREATE DATABASE IF NOT EXISTS `telefoongids` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `telefoongids`;


-- Dumping structure for table telefoongids.afdeling
CREATE TABLE IF NOT EXISTS `afdeling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='Deze tabel bevat afdeling informatie';

-- Dumping data for table telefoongids.afdeling: ~11 rows (approximately)
/*!40000 ALTER TABLE `afdeling` DISABLE KEYS */;
INSERT INTO `afdeling` (`id`, `naam`) VALUES
	(1, 'ict'),
	(2, 'hrm'),
	(3, 'beveiliging'),
	(4, 'logistiek'),
	(5, 'planning'),
	(6, 'inventarisatie'),
	(7, 'business'),
	(8, 'protocol'),
	(9, 'finance'),
	(10, 'onderhoud'),
	(12, 'Techn. Dienst');
/*!40000 ALTER TABLE `afdeling` ENABLE KEYS */;


-- Dumping structure for table telefoongids.functie
CREATE TABLE IF NOT EXISTS `functie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Deze tabel bevat de functies';

-- Dumping data for table telefoongids.functie: ~14 rows (approximately)
/*!40000 ALTER TABLE `functie` DISABLE KEYS */;
INSERT INTO `functie` (`id`, `naam`) VALUES
	(1, 'administratie'),
	(2, 'werknemer'),
	(3, 'programmer'),
	(4, 'managerL1'),
	(5, 'managerL2'),
	(6, 'afdelingshoofd'),
	(7, 'afdeling cluster hoofd'),
	(8, 'business analist'),
	(9, 'helpdesk werknemer'),
	(10, 'helpdesk manager'),
	(11, 'senoir programmer'),
	(12, 'senior business analist'),
	(13, 'afdeling beleidsadviseur'),
	(14, 'hoofd beleidsadviseur'),
	(15, 'assistent sec officer');
/*!40000 ALTER TABLE `functie` ENABLE KEYS */;


-- Dumping structure for table telefoongids.rol
CREATE TABLE IF NOT EXISTS `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Deze tabel bevat rollen welke een werknemer kan vervullen';

-- Dumping data for table telefoongids.rol: ~15 rows (approximately)
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`id`, `naam`) VALUES
	(1, 'ICT adminstrator ERP'),
	(2, 'ICT developer Portal'),
	(3, 'ICT administrator CRM'),
	(4, 'ICT Network Engineer Cisco'),
	(5, 'Procurement Officer'),
	(6, 'ICT Network Engineer Checkpoint'),
	(7, 'administratie medewerker1'),
	(8, 'administratie medewerker2'),
	(9, 'werknemer algemeen1'),
	(10, 'werknemer algemeen2'),
	(11, 'afdelingshoofd rol1'),
	(12, 'afdelingshoofd rol2'),
	(13, 'afdelingshoofd rol3'),
	(14, 'planning rol'),
	(15, 'uitvoering rol');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;


-- Dumping structure for table telefoongids.werknemer
CREATE TABLE IF NOT EXISTS `werknemer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `achternaam` varchar(32) NOT NULL,
  `voornaam` varchar(32) NOT NULL,
  `afdeling` int(11) DEFAULT NULL,
  `extentie` varchar(8) NOT NULL,
  `telefoon` varchar(32) NOT NULL,
  `functie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKafd` (`afdeling`),
  KEY `FKfunc` (`functie`),
  CONSTRAINT `FKafd` FOREIGN KEY (`afdeling`) REFERENCES `afdeling` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKfunc` FOREIGN KEY (`functie`) REFERENCES `functie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COMMENT='Dit tabel bevat de globale werknemer informatie';

-- Dumping data for table telefoongids.werknemer: ~15 rows (approximately)
/*!40000 ALTER TABLE `werknemer` DISABLE KEYS */;
INSERT INTO `werknemer` (`id`, `achternaam`, `voornaam`, `afdeling`, `extentie`, `telefoon`, `functie`) VALUES
	(1, 'ponte', 'harold', 1, '1', '441122', 1),
	(2, 'ney', 'gale', 1, '1', '441122', 2),
	(3, 'dossey', 'israel', 2, '2', '441123', 2),
	(4, 'jenning', 'jarod', 3, '3', '441133', 4),
	(5, 'larosa', 'milton', 4, '4', '442244', 6),
	(6, 'vose', 'derek', 6, '6', '441126', 6),
	(7, 'pocock', 'shoanne', 1, '1', '441122', 6),
	(8, 'jacobsen', 'lucy', 1, '1', '441122', 7),
	(9, 'pocock', 'ann', 1, '1', '441122', 6),
	(10, 'jacobsen', 'trina', 1, '1', '441122', 7),
	(11, 'sparling', 'ann', 2, '1', '441122', 3),
	(12, 'rayburn', 'trina', 2, '1', '441122', 3),
	(13, 'bautch', 'beth', 3, '1', '441122', 3),
	(14, 'lam', 'rosa', 3, '1', '441122', 3),
	(19, 'Dedoper', 'Johannes', 1, '3420', '082669832', 3);
/*!40000 ALTER TABLE `werknemer` ENABLE KEYS */;


-- Dumping structure for table telefoongids.werknemerrol
CREATE TABLE IF NOT EXISTS `werknemerrol` (
  `werknemerid` int(11) NOT NULL,
  `rolid` int(11) NOT NULL,
  KEY `FKwerknemer` (`werknemerid`),
  KEY `FKrol` (`rolid`),
  CONSTRAINT `FKrol` FOREIGN KEY (`rolid`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKwerknemer` FOREIGN KEY (`werknemerid`) REFERENCES `werknemer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Dit is de tussenentiteit welke de many-to-many relatie vathoudt tussen Werknemer en Rol tabel';

-- Dumping data for table telefoongids.werknemerrol: ~32 rows (approximately)
/*!40000 ALTER TABLE `werknemerrol` DISABLE KEYS */;
INSERT INTO `werknemerrol` (`werknemerid`, `rolid`) VALUES
	(1, 8),
	(1, 7),
	(1, 3),
	(2, 7),
	(2, 9),
	(2, 15),
	(3, 1),
	(3, 4),
	(3, 5),
	(4, 1),
	(4, 2),
	(4, 3),
	(4, 4),
	(4, 5),
	(5, 1),
	(5, 2),
	(5, 3),
	(5, 4),
	(5, 5),
	(7, 1),
	(7, 2),
	(7, 3),
	(7, 11),
	(7, 12),
	(7, 7),
	(7, 10),
	(8, 10),
	(8, 11),
	(8, 12),
	(9, 4),
	(9, 5),
	(9, 6);
/*!40000 ALTER TABLE `werknemerrol` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
